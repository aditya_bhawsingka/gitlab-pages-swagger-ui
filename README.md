# Example Swagger UI API documentation site using GitLab Pages.

Learn more about GitLab Pages at https://pages.gitlab.io and the official documentation https://docs.gitlab.com/ce/user/project/pages/

Learn more about Swagger UI at https://github.com/swagger-api/swagger-ui/

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```yaml
image: node:10-alpine

variables:
  DOCS_FOLDER: "api-docs"
  SPEC_TO_DISPLAY: "my-project_specification_0.0.1.json"

cache:
  paths:
    - ./node_modules

pages:
  stage: deploy
  before_script:
    - npm install swagger-ui-dist@3.22.1
  script:
    - mkdir public
    - cp -rp node_modules/swagger-ui-dist/* public
    - cp -rp $DOCS_FOLDER/* public
    - sed -i "s#https://petstore\.swagger\.io/v2/swagger\.json#$SPEC_TO_DISPLAY#g" public/index.html
  artifacts:
    paths:
      - public
  only:
    - master
```

The above example expects that all the OpenAPI documentation files are in the `api-docs/` directory, 
and that the specification to be displayed is `my-project_specification_0.0.1.json`.

All files form the `api-docs/` folder will be available to be viewed on the page given the correct filename.

[ci]: https://about.gitlab.com/gitlab-ci/

### Adjustments
For example, if you would like to display the file that was most recently modified 
from the `$DOCS_FOLDER`, you could do this in script:
```yaml
    - export SPEC_TO_DISPLAY=$(ls -t $DOCS_FOLDER | head -n1)
```

Or, if you would like to have a cleaner artifact, you could remove the unnecessary 
files that were copied from `swagger-ui-dist` to `public`:
```yaml
    - rm public/package.json public/index.js public/absolute-path.js public/README.md public/swagger-ui.js public/swagger-ui.js.map
```

## Gitlab Pages

The resulting page will be available on the standard GitLab Pages subdomain.

For the results of this example visit https://mdhtr.gitlab.io/gitlab-pages-swagger-ui/